﻿using System;

namespace DayOfTheProgrammer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(LeapOrNot(1800));
        }

        static string dayOfProgrammer(int year) {
        if(year == 1918){
            return "26.09.1918";
        }else if((year <= 1917) && (year % 4 == 0) || ((year > 1918) && LeapOrNot(year))){
            return $"12.09.{year}";
        } else{
            return $"13.09.{year}";
        }
    }

    static bool LeapOrNot(int year){
        return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
    }



    }
}
